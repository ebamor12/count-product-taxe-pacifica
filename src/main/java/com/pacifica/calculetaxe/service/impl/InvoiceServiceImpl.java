package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.FileUtil;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.InvoiceService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.InvoiceConstant.*;
import static com.pacifica.calculetaxe.common.constant.LogConstant.*;
import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.A_IN_LINE_ORDER;
import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.EURO_IN_LINE_ORDER;


public class InvoiceServiceImpl implements InvoiceService {

    private static final Logger LOGGER = Logger.getLogger(InvoiceServiceImpl.class.getName());

    @Override
    public Invoice createInvoiceFromOrder(Order order, InvoiceDetailsService invoiceDetailsService, TaxeCalculatorService taxeCalculatorService) {
        Invoice invoice = new Invoice();
        List<InvoiceDetails> invoiceDetailsList = order.getOrderDetails()
                .stream()
                .map(orderDetails -> (invoiceDetailsService.createInvoiceDetail(orderDetails, taxeCalculatorService)))
                .collect(Collectors.toList());
        invoice.setInvoiceDetails(invoiceDetailsList);
        invoice.setTotalProductsTaxe(taxeCalculatorService.countTotalTax(order.getOrderDetails()));
        invoice.setTotaleProductsPrices(taxeCalculatorService.countTotalPriceTTC(order.getOrderDetails()));
        return invoice;
    }

    @Override
    public String createInvoiceFile(Invoice invoice) throws IOException {
        LOGGER.info(CREATE_INVOICE_FILE_LOG);
        File file = FileUtil.createNewFile(invoice);
        if (file.createNewFile()) {
            FileUtil.createTextInFile(file, this.createInvoiceText(invoice));
        } else {
            LOGGER.info(FILE_EXIST_LOG + file.getCanonicalPath());
        }
        return file.getCanonicalPath();
    }

    private String createInvoiceText(Invoice invoice) {
        String invoiceText;
        if (invoice != null) {
            List<String> totalPriceAndTaxeList = new ArrayList<>();
            totalPriceAndTaxeList.add(createTotalTaxeText(invoice));
            totalPriceAndTaxeList.add(createTotalPriceText(invoice));
            String totalPriceAndTax = FileUtil.joinString(totalPriceAndTaxeList, DELIMETER_ONE_LINE, EMPTY_STRING, EMPTY_STRING);
            String productLines = FileUtil.joinString(createInvoiceLinesText(invoice), DELIMETER_ONE_LINE, EMPTY_STRING, EMPTY_STRING);
            List<String> finalStrList = new ArrayList<>();
            finalStrList.add(productLines);
            finalStrList.add(totalPriceAndTax);
            invoiceText = FileUtil.joinString(finalStrList, DELIMETER_TWO_LINE, EMPTY_STRING, EMPTY_STRING);
        } else {
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        }
        return invoiceText;
    }

    private String createTotalTaxeText(Invoice invoice) {
        List<String> totalTaxAndEuro = new ArrayList<>();
        totalTaxAndEuro.add(TAXE_TOTAL);
        totalTaxAndEuro.add(invoice.getTotalProductsTaxe().toString());
        totalTaxAndEuro.add(EURO_IN_LINE_ORDER);
        return FileUtil.joinString(totalTaxAndEuro, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
    }

    private String createTotalPriceText(Invoice invoice) {
        List<String> totalPriceAndEuro = new ArrayList<>();
        totalPriceAndEuro.add(TOTAL_PRIX);
        totalPriceAndEuro.add(invoice.getTotaleProductsPrices().toString());
        totalPriceAndEuro.add(EURO_IN_LINE_ORDER);
        return FileUtil.joinString(totalPriceAndEuro, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
    }

    private List<String> createInvoiceLinesText(Invoice invoice) {
        List<String> invoiceProductLine = new ArrayList<>();
        if (invoice != null) {
            invoiceProductLine = invoice.getInvoiceDetails()
                    .stream()
                    .map(this::createInvoiceLineText)
                    .collect(Collectors.toList());
        }
        return invoiceProductLine;
    }

    private String createInvoiceLineText(InvoiceDetails invoiceDetail) {
        List<String> invoiceLineTextList = new ArrayList<>();
        invoiceLineTextList.add(START_INVOICE_STRING);
        invoiceLineTextList.add(invoiceDetail.getQuantity().toString());
        invoiceLineTextList.add(invoiceDetail.getProduct().getLibelle());
        if (!invoiceDetail.getImportLibelle().equals(EMPTY_STRING)) {
            invoiceLineTextList.add(invoiceDetail.getImportLibelle());
        }
        invoiceLineTextList.add(A_IN_LINE_ORDER);
        invoiceLineTextList.add(createPriceTextText(invoiceDetail.getProduct().getUnitPriceWithoutTaxe().toString(), EURO_IN_INVOICE));
        invoiceLineTextList.add(createPriceTextText(invoiceDetail.getPriceTTC().toString(), EURO_IN_LINE_ORDER));
        return FileUtil.joinString(invoiceLineTextList, SPACE_STRING, EMPTY_STRING, EMPTY_STRING).trim();
    }

    private String createPriceTextText(String priceText, String euroText) {
        List<String> uniProductPrice = new ArrayList<>();
        uniProductPrice.add(priceText);
        uniProductPrice.add(euroText);
        return FileUtil.joinString(uniProductPrice, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
    }


}
