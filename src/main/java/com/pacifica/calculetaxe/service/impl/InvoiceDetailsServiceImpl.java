package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;

public class InvoiceDetailsServiceImpl implements InvoiceDetailsService {

    @Override
    public InvoiceDetails createInvoiceDetail(OrderDetails orderDetail, TaxeCalculatorService taxeCalculatorService) {
        InvoiceDetails invoiceDetail = new InvoiceDetails();
        invoiceDetail.setPriceTTC(taxeCalculatorService.countProductPriceTTC(orderDetail));
        invoiceDetail.setProduct(orderDetail.getProduct());
        invoiceDetail.setQuantity(orderDetail.getQuantity());
        invoiceDetail.setImportLibelle(orderDetail.getImportLibelle());
        return invoiceDetail;
    }
}
