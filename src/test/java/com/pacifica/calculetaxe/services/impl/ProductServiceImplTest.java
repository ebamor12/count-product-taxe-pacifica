package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ProductServiceImplTest {

    ProductServiceImpl productServiceImpl;

    @Before
    public void setUp() {
        productServiceImpl = new ProductServiceImpl();
    }

    @Test
    public void should_create_book_product_from_orderline() {
        String orderLine = "* 2 livres à 12.49€";
        Integer quantity = 2;
        Product expectedProduct = TestUtils.getBookProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_other_product() {
        String orderLine = "* 1 CD musical à 14.99€";
        Integer quantity = 1;
        Product expectedProduct = TestUtils.getOthersProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_food_product() {
        String orderLine = "* 3 barres de chocolat à 0.85€";
        Integer quantity = 3;
        Product expectedProduct = TestUtils.getChocolateProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_medication_product() {
        String orderLine = "* 3 boîtes de pilules contre la migraine à 9.75€";
        Integer quantity = 3;
        Product expectedProduct = TestUtils.getPillProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_other_product_when_product_name_dosent_exist() {
        String orderLine = "* 3 casques à 9.75€";
        Integer quantity = 3;
        Product expectedProduct = TestUtils.getOtherProduct();
        Product product = productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(expectedProduct, product);
    }
}
