package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.OrderDetailsService;
import com.pacifica.calculetaxe.service.ProductService;
import com.pacifica.calculetaxe.service.impl.OrderDetailsServiceImpl;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OrderDetailsServiceImplTest {
    OrderDetailsService orderDetailsService;

    @Before
    public void setUp() {
        orderDetailsService = new OrderDetailsServiceImpl();
    }

    @Test
    public void should_create_orderDetail_from_orderline() {
        String orderLine = "* 2 livres à 12.49€";
        ProductService productService = new ProductServiceImpl();
        OrderDetails expectedOrderDetails = TestUtils.getFirstOrderLine();
        OrderDetails orderDetails = orderDetailsService.createOrderDetailFromOrderLine(orderLine, productService);
        Assert.assertEquals(expectedOrderDetails, orderDetails);
    }
}
