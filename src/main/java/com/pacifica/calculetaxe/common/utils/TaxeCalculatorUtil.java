package com.pacifica.calculetaxe.common.utils;

import java.math.BigDecimal;

import static com.pacifica.calculetaxe.common.constant.ValueConstant.FIVE_CENT;
import static com.pacifica.calculetaxe.common.constant.ValueConstant.HUNDRED;

public final class TaxeCalculatorUtil {

    public static BigDecimal countTaxe(BigDecimal value, int percent) {
        return (value.multiply(BigDecimal.valueOf(percent))).divide(BigDecimal.valueOf(HUNDRED));
    }

    public static BigDecimal roundToNearestFiveCents(BigDecimal value) {
        BigDecimal remainder = value.remainder(FIVE_CENT);
        return (BigDecimal.ZERO.compareTo(remainder) == 0) ? value : (value.add(FIVE_CENT).subtract(value.remainder(FIVE_CENT)));
    }

    private TaxeCalculatorUtil() {
    }
}
