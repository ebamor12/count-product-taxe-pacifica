package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.models.Invoice;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.FileConstant.*;
import static com.pacifica.calculetaxe.common.constant.InvoiceConstant.EMPTY_STRING;
import static com.pacifica.calculetaxe.common.constant.LogConstant.INVOICE_NULL_LOG;

public final class FileUtil {

    public static File createNewFile(Invoice invoice) {
        if (invoice != null) {
            return new File(generateFileName(invoice.getTotaleProductsPrices().toString()));
        } else {
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        }
    }

    private static String generateFileName(String totalPrices) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        List<String> fileNameList = new ArrayList<>();
        fileNameList.add(TEST_PATH);
        fileNameList.add(FILE_NAME);
        fileNameList.add(SEPARATION);
        fileNameList.add(totalPrices);
        fileNameList.add(dateFormat.format(date));
        fileNameList.add(TXT_EXTENSION);
        return joinString(fileNameList, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
    }

    public static void createTextInFile(File file, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(text);
        printWriter.close();
    }

    public static String joinString(List<String> stringToAdd, String delimiter, String prefix, String suffix) {
        StringJoiner productLinesJoiner = new StringJoiner(delimiter, prefix, suffix);
        stringToAdd.stream()
                .map(line -> productLinesJoiner.add(line))
                .collect(Collectors.toList());
        return productLinesJoiner.toString();
    }

    private FileUtil() {
    }
}
