package com.pacifica.calculetaxe.models;

import java.math.BigDecimal;
import java.util.Objects;

public class InvoiceDetails extends OrderDetails {

    private BigDecimal priceTTC;

    public BigDecimal getPriceTTC() {
        return priceTTC;
    }

    public void setPriceTTC(BigDecimal priceTTC) {
        this.priceTTC = priceTTC;
    }

    @Override
    public String toString() {
        return "InvoiceDetails{" +
                "priceTTC=" + priceTTC +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InvoiceDetails)) return false;
        InvoiceDetails that = (InvoiceDetails) o;
        return Objects.equals(getPriceTTC(), that.getPriceTTC());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPriceTTC());
    }
}