package com.pacifica.calculetaxe.models;

import java.util.List;
import java.util.Objects;

public class Order {

    private List<OrderDetails> orderDetails;

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Order(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderDetails=" + orderDetails +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(getOrderDetails(), order.getOrderDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrderDetails());
    }
}
